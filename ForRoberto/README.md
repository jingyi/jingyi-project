# Guidence
Please follow the order of the guidence list.

# Fiducial region  
1. Use TLorentzVector 
2. Please see add_ETA_Omegacc.C for eta and add_Y_Omegacc.C for y.

# Turbo selection
1. Need to apply Turbo cuts offline, so only TRUEID-Matching for MC 
2. Please see DaVinci_OmegaccP2XicKPi_160fs_MCTruth.py 

# Use Gauss to generate MC samples offline 
1. Generate_level only, no reconstruction from detectors
2. Set Gauss environment first, please follow the introduction https://lhcb.github.io/starterkit-lessons/second-analysis-steps/simulation.html
3. I would perform examples as the following two, for acceptance efficiency evaluation and different mass hypotheses  

# Acceptance efficiency
1. The efficiency for particles generate in acceptance of LHCb detector
1. See codes in 4pi/.

# Mass hypotheses
1. Change the input mass and do the simulation
1. See codes in mass/ 
  
# Reweight
1. Get the weights and apply the weights to your samples 
2. Kinematic reweight and mass reweight
2. See codes in reweight/

# PIDCalib
1. Please find the information from https://twiki.cern.ch/twiki/bin/view/LHCb/PIDCalibPackage
2. See codes in PIDCalib/
3. Add weights or evaluate the PID efficiencies directly by codes in /PIDCalib/add_weight/.


# Tracking correction
1. Read the correction table privided by Tracking Group.
2. The tracking correction would effect the efficiencies of selection and reconstruction.
3. Ratio_Long_P-ETA_2016_25ns.root is for 2016.
