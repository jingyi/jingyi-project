# Kinematic reweight
1. Apply the discrenpancy between signal and simulation (obtained from your control mode) to your signal mode
2. Get the discrenpancy from the ratio of the histograms (binning)
3. reweightpt_Omegacc.C, reweight for transverse momentum
4. reweightspd_Omegacc.C, reweight multiplicity 
5. reweighty_Omegacc.C, reweight rapidity, but the y would probably be good enough after finishing the previous two steps, check it and decide if you really need reweight rapidity  

# Mass hyputheses
1. Use GBReweigher method
2. Only need to reweight PT of your final states
3. Omegacc.py, the GBReweigher, to obtain the discrepancy of "fixdata" and “data2reweight”  then make weights for "datatoaddweight" saved in "newfile2saveweight"
4. adding_weights_to_root_file.py, add the weights (saved in "input_file_w") as a branch of the target root ("input_file") with branch named "branch_w" 
