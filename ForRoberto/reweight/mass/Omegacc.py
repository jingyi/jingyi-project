from reweight import Gbreweighter 
from ROOT import TFile, TH2D, TH1D, TCanvas 
import numpy as np

data2reweight = "root/3738_inFiducial.root"
datatoaddweight = "root/OmegaccMC16afweight_new.root"
#fixdata = "root/3638_inFiducial.root"
fixdata = "root/3838_inFiducial.root"
newfile2saveweight = "3838_MCD.root"
#newfile2saveweight = "3638_MCD.root"
#newfile2saveweight = "3838_DT.root"
#newfile2saveweight = "3638_DT.root"
used_branch = ['Lc_TRUEPT','KM_TRUEPT','PiP_TRUEPT']
denominator_weight = None
numerator_weight = None

Gbreweighter.GBreweighter_get_add_weights(newfile2saveweight, used_branch, data2reweight, fixdata, datatoaddweight, denominator_weight, numerator_weight, "3838_MCD.pdf")
