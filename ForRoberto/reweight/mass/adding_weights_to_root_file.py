#===========by Miroslav!!!!!!!!!!!!!===============#
import sys, os
import ROOT
import numpy as np
import root_numpy as r_np

import pandas, root_pandas
from root_pandas import read_root, to_root

input_file = 'root/OmegaccMC16afweight_new.root' #original ROOT file which you want to append
input_tree = 'MCDecayTree' #original TTree which you want to append

input_file_w = '3638_MCD.root' #ROOT file with kinematic weights !!!MUST HAVE A SAME NUMBER OF EVENTS AS ORIGINAL FILE!!!
input_tree_w = 'MCDecayTree' #TTree containing weights

branch_w = ['weight_3638'] 

dataset = r_np.root2array(input_file_w, input_tree_w, branch_w)

r_np.array2root(dataset, input_file, treename=input_tree, mode='update')
