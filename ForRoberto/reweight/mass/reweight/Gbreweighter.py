import numpy
import root_numpy
import pandas, root_pandas   #datasets which are nicer.
from root_pandas import read_root
from hep_ml import reweight
from matplotlib import pyplot as plt
from hep_ml.metrics_utils import ks_2samp_weighted


#draw function define (to check the weight results)
def draw_distributions(original, target, var2reweight, new_original_weights, splot_weights, plotsname):
    hist1_settings = {'bins': 40, 'density': True, 'alpha': 0.7}
    plt.figure(figsize=[15, 7])
    for id, column in enumerate(var2reweight, 1):
        xlim = numpy.percentile(numpy.hstack([target[column]]), [0.01, 99.99])
        plt.subplot(2, 3, id)
        plt.hist(original[column], weights=new_original_weights, range=xlim, **hist1_settings, label="Weighted")
        plt.hist(target[column], weights=splot_weights,range=xlim, **hist1_settings,label="Target")
        handles,labels=plt.gca().get_legend_handles_labels()
        plt.legend(loc='best')
        plt.title(column)
        print('KS over ', column, ' = ', ks_2samp_weighted(original[column], target[column],weights1=new_original_weights, weights2=splot_weights))
    plt.savefig(plotsname)
##    plt.show()




		

def GBreweighter_get_add_weights(newfile_withweight, var2reweight, root2reweight, rootfixdata, root2addweight, orignalweights, targetweights, plotsname):
#newfile_with weight is the root file you want to save the weight to (adding weights with root2addweight file)
#root2reweight is the denominator
#fixdate is the numerator
#root2addweight is the root file you want to add weight

    print('the reweight variable are ',' :  ',var2reweight)
#    all_branch = root_numpy.list_branches(root2addweight)
#    all_branch = root_numpy.list_branches(root2addweight,'MCDecayTree')
    original = root_numpy.root2array(root2reweight, branches=var2reweight)
    target = root_numpy.root2array(rootfixdata, branches=var2reweight)
#    used = root_numpy.root2array(root2addweight,'DecayTree', branches=var2reweight)
    used = root_numpy.root2array(root2addweight,'MCDecayTree', branches=var2reweight)

    original = pandas.DataFrame(original)
    target = pandas.DataFrame(target)
    used = pandas.DataFrame(used)
#    used_tmp = read_root(root2addweight,'MCDecayTree',columns=all_branch)


#set up the orignal weights (weights for the orignal file and the target files)

    if orignalweights is None:
        original_weights = numpy.ones(len(original))  
    else:
        original_weights = root_numpy.root2array(root2reweight, branches=orignalweights)

    if targetweights is None:
        target_weights = numpy.ones(len(target))
    else:
        target_weights = root_numpy.root2array(rootfixdata, branches=targetweights)



    reweighter = reweight.GBReweighter(n_estimators=70, learning_rate=0.1, max_depth=3, min_samples_leaf=100, gb_args={'subsample': 0.7})
    reweighter.fit(original, target, original_weights, target_weights)
    
    gb_weights_test = reweighter.predict_weights(original)
    gb_weights_used = reweighter.predict_weights(used)
    print("The reweighting is done, if there's no error, saving the comparison plots to the plots folder")
   # print(type(gb_weights_used))

#reweighting done
#show the weight results
#validate reweighting vars on the test part comparing 1d projections

    draw_distributions(original, target, var2reweight, gb_weights_test, target_weights, plotsname)


#saving weight to root


    weighttree = 'MCDecayTree'
#    weighttree = 'DecayTree'
    weightname = 'weight_3838'
#    weightname = 'weight_3638'
#    used_tmp[weightname]=gb_weights_used
#    used_tmp.to_root( newfile_withweight,weighttree)
    used[weightname]=gb_weights_used
    used.to_root( newfile_withweight,weighttree)

