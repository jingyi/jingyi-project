# file /publicfs/ucas/user/xujingyi/cmtuser/GaussDev_v49r12/Gen/DecFiles/options/26165856.py generated: Tue, 26 Feb 2019 20:18:50
#
# Event Type: 26165856
#
# ASCII decay Descriptor: [Omega_cc+ -> (Xi_c+ -> p+ K- pi+) K- pi+ ]cc
#
from Configurables import Generation
Generation().EventType = 26165856
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "GenXiccProduction"
Generation().PileUpTool = "FixedLuminosityForRareProcess"
from Configurables import GenXiccProduction
Generation().Special.addTool( GenXiccProduction )
Generation().Special.GenXiccProduction.BaryonState = "Omega_cc+"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Omegacc_Xic+Kpi,pKpi-res=GenXicc,DecProdCut,WithMinPTv1.dec"
Generation().Special.CutTool = "XiccDaughtersInLHCbAndWithMinPT"
from Configurables import XiccDaughtersInLHCbAndWithMinPT
Generation().Special.addTool( XiccDaughtersInLHCbAndWithMinPT )
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.BaryonState = Generation().Special.GenXiccProduction.BaryonState
from GaudiKernel import SystemOfUnits
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.MinXiccPT = 0*SystemOfUnits.MeV
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.ChargedThetaMin = 0*SystemOfUnits.mrad
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.ChargedThetaMax = 3141.592653*SystemOfUnits.mrad
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.NeutralThetaMin = 0*SystemOfUnits.mrad
Generation().Special.XiccDaughtersInLHCbAndWithMinPT.NeutralThetaMax = 3141.592653*SystemOfUnits.mrad
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ "Omega_cc+ 510 4432 1.0 3.638 1.60e-13 Omega_cc+ 4432 0.", "Omega_cc~- 511 -4432 -1.0 3.638 1.60e-13 anti-Omega_cc- -4432 0." ]
