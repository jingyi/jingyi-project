################
# CONFIGURATION
################
EventType = '26165856'
DecayDescriptor = '[Omega_cc+ =>^( Xi_c+ ==>^p+ ^K- ^pi+ ) ^pi+ ^K-]CC'
#DecayDescriptor = '[ Xi_cc++ => ^(Lambda_c+ ==> ^p+ ^K- ^pi+) ^K- ^pi+ ^pi+ ]CC'
DecayHeader = 'Omega_cc++,Omega_cc--'

################
#import sys, os
from DaVinci.Configuration import *
from Gaudi.Configuration import *

from Configurables import DaVinci, PrintMCTree
from Configurables import MCDecayTreeTuple, MCTupleToolKinematic, MCTupleToolHierarchy, LoKi__Hybrid__MCTupleTool

mctuple = MCDecayTreeTuple( 'MCDecayTreeTuple' )
mctuple.Decay = DecayDescriptor
mctuple.Branches = {

   "LcP" :"[Omega_cc+ =>( Xi_c+ ==>^p+ K- pi+ ) pi+ K-]CC",
   "LcK" :"[Omega_cc+ =>( Xi_c+ ==>p+ ^K- pi+ ) pi+ K-]CC",
   "LcPi":"[Omega_cc+ =>( Xi_c+ ==>p+ K- ^pi+ ) pi+ K-]CC",
   "Lc"  :"[Omega_cc+ =>^(Xi_c+ ==>p+ K- pi+ ) pi+ K-]CC",
   "KM"  :"[Omega_cc+ =>(Xi_c+ ==>p+ K- pi+ ) pi+ ^K-]CC",
   "PiP" :"[Omega_cc+ =>(Xi_c+ ==>p+ K- pi+ ) ^pi+ K-]CC",
   "C"   :"[Omega_cc+ =>(Xi_c+ ==>p+ K- pi+ ) pi+ K-]CC"

}
mctuple.ToolList = [ "MCTupleToolHierarchy"
                                          , "MCTupleToolKinematic"
                                          , "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
                                          ]

mctuple.addTool(MCTupleToolKinematic())
mctuple.MCTupleToolKinematic.Verbose=True


# LoKi variables
LoKi_Photos = LoKi__Hybrid__MCTupleTool("LoKi_Photos")
LoKi_Photos.Variables = {
    "nPhotos"  : "MCNINTREE ( ('gamma'==MCABSID) )"
        }
mctuple.addTool(LoKi_Photos)

"""
Print MC Tree
"""

DaVinci().TupleFile  = '3738.root'
#'tupleProduction-'+EventType+'.root'
DaVinci().EvtMax     = -1
DaVinci().Simulation = True
DaVinci().Lumi       = False
DaVinci().InputType  = "DST"
DaVinci().DataType   = "2016"
#DaVinci().CondDBtag  = "MC2011-20120727-vc-md100"
#DaVinci().DDDBtag    = "MC2011-20120727"
DaVinci().DDDBtag   = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
#DaVinci().CondDBtag = "sim-20180411-vc-md100"
#DaVinci().CondDBtag = "sim-20130222-1-vc-md100"
#DaVinci().DDDBtag   = "dddb-20130312-1"
DaVinci().UserAlgorithms = [ mctuple ]

EventSelector().Input = ["DATAFILE='Gauss-26165856-50000ev-20190617.xgen'"]
