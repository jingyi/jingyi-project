# Acceptance efficiency
1. Already prepared three projects of different mass hypotheses, the nominal one is 3738 (for Omegacc)
2. cd 3738/, the command lines are in the gen.sh, where all python files are the option for the generation
3. 26165856.py, the option file of the decayfile named by the EventType, line 27-30 perfomed the requirement of the topological angle of the decay, which is 4pi space. More information of the decayfile please see https://lhcb.github.io/starterkit-lessons/second-analysis-steps/simulation-dkfiles.html
4. Gauss-2016.py, beam, energy and the other data option for the year of 2016
5. GenXiccPythia8.py, specific generator for doubly heavy baryon
6. GenStandAlone.py, fast but only generate level, remove this file would make the generation full-simulated and very slow
7. Gauss-Job.py, number of events you would like to generate, the ramdom seed
8. Source the gen.sh and get the .xgen file.
9. Run Davinci.py to get the root.
10. Use Stripping_Turbo_CDec.C to calculate the acceptance efficiency.
12. Do the same process for the other mass hypotheses
