//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 31 00:34:26 2020 by ROOT version 5.34/18
// from TTree MCDecayTree/MCDecayTree
// found on file: 3638.root
//////////////////////////////////////////////////////////

#ifndef MCDecayTree_h
#define MCDecayTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class MCDecayTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        C_nPhotos;
   Int_t           C_MC_MOTHER_ID;
   Int_t           C_MC_MOTHER_KEY;
   Int_t           C_MC_GD_MOTHER_ID;
   Int_t           C_MC_GD_MOTHER_KEY;
   Int_t           C_MC_GD_GD_MOTHER_ID;
   Int_t           C_MC_GD_GD_MOTHER_KEY;
   Double_t        C_TRUEP_E;
   Double_t        C_TRUEP_X;
   Double_t        C_TRUEP_Y;
   Double_t        C_TRUEP_Z;
   Double_t        C_TRUEPT;
   Double_t        C_TRUEORIGINVERTEX_X;
   Double_t        C_TRUEORIGINVERTEX_Y;
   Double_t        C_TRUEORIGINVERTEX_Z;
   Double_t        C_TRUEENDVERTEX_X;
   Double_t        C_TRUEENDVERTEX_Y;
   Double_t        C_TRUEENDVERTEX_Z;
   Bool_t          C_TRUEISSTABLE;
   Double_t        C_TRUETAU;
   Bool_t          C_OSCIL;
   Double_t        Lc_nPhotos;
   Int_t           Lc_MC_MOTHER_ID;
   Int_t           Lc_MC_MOTHER_KEY;
   Int_t           Lc_MC_GD_MOTHER_ID;
   Int_t           Lc_MC_GD_MOTHER_KEY;
   Int_t           Lc_MC_GD_GD_MOTHER_ID;
   Int_t           Lc_MC_GD_GD_MOTHER_KEY;
   Double_t        Lc_TRUEP_E;
   Double_t        Lc_TRUEP_X;
   Double_t        Lc_TRUEP_Y;
   Double_t        Lc_TRUEP_Z;
   Double_t        Lc_TRUEPT;
   Double_t        Lc_TRUEORIGINVERTEX_X;
   Double_t        Lc_TRUEORIGINVERTEX_Y;
   Double_t        Lc_TRUEORIGINVERTEX_Z;
   Double_t        Lc_TRUEENDVERTEX_X;
   Double_t        Lc_TRUEENDVERTEX_Y;
   Double_t        Lc_TRUEENDVERTEX_Z;
   Bool_t          Lc_TRUEISSTABLE;
   Double_t        Lc_TRUETAU;
   Bool_t          Lc_OSCIL;
   Double_t        LcP_nPhotos;
   Int_t           LcP_MC_MOTHER_ID;
   Int_t           LcP_MC_MOTHER_KEY;
   Int_t           LcP_MC_GD_MOTHER_ID;
   Int_t           LcP_MC_GD_MOTHER_KEY;
   Int_t           LcP_MC_GD_GD_MOTHER_ID;
   Int_t           LcP_MC_GD_GD_MOTHER_KEY;
   Double_t        LcP_TRUEP_E;
   Double_t        LcP_TRUEP_X;
   Double_t        LcP_TRUEP_Y;
   Double_t        LcP_TRUEP_Z;
   Double_t        LcP_TRUEPT;
   Double_t        LcP_TRUEORIGINVERTEX_X;
   Double_t        LcP_TRUEORIGINVERTEX_Y;
   Double_t        LcP_TRUEORIGINVERTEX_Z;
   Double_t        LcP_TRUEENDVERTEX_X;
   Double_t        LcP_TRUEENDVERTEX_Y;
   Double_t        LcP_TRUEENDVERTEX_Z;
   Bool_t          LcP_TRUEISSTABLE;
   Double_t        LcP_TRUETAU;
   Bool_t          LcP_OSCIL;
   Double_t        LcK_nPhotos;
   Int_t           LcK_MC_MOTHER_ID;
   Int_t           LcK_MC_MOTHER_KEY;
   Int_t           LcK_MC_GD_MOTHER_ID;
   Int_t           LcK_MC_GD_MOTHER_KEY;
   Int_t           LcK_MC_GD_GD_MOTHER_ID;
   Int_t           LcK_MC_GD_GD_MOTHER_KEY;
   Double_t        LcK_TRUEP_E;
   Double_t        LcK_TRUEP_X;
   Double_t        LcK_TRUEP_Y;
   Double_t        LcK_TRUEP_Z;
   Double_t        LcK_TRUEPT;
   Double_t        LcK_TRUEORIGINVERTEX_X;
   Double_t        LcK_TRUEORIGINVERTEX_Y;
   Double_t        LcK_TRUEORIGINVERTEX_Z;
   Double_t        LcK_TRUEENDVERTEX_X;
   Double_t        LcK_TRUEENDVERTEX_Y;
   Double_t        LcK_TRUEENDVERTEX_Z;
   Bool_t          LcK_TRUEISSTABLE;
   Double_t        LcK_TRUETAU;
   Bool_t          LcK_OSCIL;
   Double_t        LcPi_nPhotos;
   Int_t           LcPi_MC_MOTHER_ID;
   Int_t           LcPi_MC_MOTHER_KEY;
   Int_t           LcPi_MC_GD_MOTHER_ID;
   Int_t           LcPi_MC_GD_MOTHER_KEY;
   Int_t           LcPi_MC_GD_GD_MOTHER_ID;
   Int_t           LcPi_MC_GD_GD_MOTHER_KEY;
   Double_t        LcPi_TRUEP_E;
   Double_t        LcPi_TRUEP_X;
   Double_t        LcPi_TRUEP_Y;
   Double_t        LcPi_TRUEP_Z;
   Double_t        LcPi_TRUEPT;
   Double_t        LcPi_TRUEORIGINVERTEX_X;
   Double_t        LcPi_TRUEORIGINVERTEX_Y;
   Double_t        LcPi_TRUEORIGINVERTEX_Z;
   Double_t        LcPi_TRUEENDVERTEX_X;
   Double_t        LcPi_TRUEENDVERTEX_Y;
   Double_t        LcPi_TRUEENDVERTEX_Z;
   Bool_t          LcPi_TRUEISSTABLE;
   Double_t        LcPi_TRUETAU;
   Bool_t          LcPi_OSCIL;
   Double_t        PiP_nPhotos;
   Int_t           PiP_MC_MOTHER_ID;
   Int_t           PiP_MC_MOTHER_KEY;
   Int_t           PiP_MC_GD_MOTHER_ID;
   Int_t           PiP_MC_GD_MOTHER_KEY;
   Int_t           PiP_MC_GD_GD_MOTHER_ID;
   Int_t           PiP_MC_GD_GD_MOTHER_KEY;
   Double_t        PiP_TRUEP_E;
   Double_t        PiP_TRUEP_X;
   Double_t        PiP_TRUEP_Y;
   Double_t        PiP_TRUEP_Z;
   Double_t        PiP_TRUEPT;
   Double_t        PiP_TRUEORIGINVERTEX_X;
   Double_t        PiP_TRUEORIGINVERTEX_Y;
   Double_t        PiP_TRUEORIGINVERTEX_Z;
   Double_t        PiP_TRUEENDVERTEX_X;
   Double_t        PiP_TRUEENDVERTEX_Y;
   Double_t        PiP_TRUEENDVERTEX_Z;
   Bool_t          PiP_TRUEISSTABLE;
   Double_t        PiP_TRUETAU;
   Bool_t          PiP_OSCIL;
   Double_t        KM_nPhotos;
   Int_t           KM_MC_MOTHER_ID;
   Int_t           KM_MC_MOTHER_KEY;
   Int_t           KM_MC_GD_MOTHER_ID;
   Int_t           KM_MC_GD_MOTHER_KEY;
   Int_t           KM_MC_GD_GD_MOTHER_ID;
   Int_t           KM_MC_GD_GD_MOTHER_KEY;
   Double_t        KM_TRUEP_E;
   Double_t        KM_TRUEP_X;
   Double_t        KM_TRUEP_Y;
   Double_t        KM_TRUEP_Z;
   Double_t        KM_TRUEPT;
   Double_t        KM_TRUEORIGINVERTEX_X;
   Double_t        KM_TRUEORIGINVERTEX_Y;
   Double_t        KM_TRUEORIGINVERTEX_Z;
   Double_t        KM_TRUEENDVERTEX_X;
   Double_t        KM_TRUEENDVERTEX_Y;
   Double_t        KM_TRUEENDVERTEX_Z;
   Bool_t          KM_TRUEISSTABLE;
   Double_t        KM_TRUETAU;
   Bool_t          KM_OSCIL;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;

   // List of branches
   TBranch        *b_C_nPhotos;   //!
   TBranch        *b_C_MC_MOTHER_ID;   //!
   TBranch        *b_C_MC_MOTHER_KEY;   //!
   TBranch        *b_C_MC_GD_MOTHER_ID;   //!
   TBranch        *b_C_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_C_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_C_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_C_TRUEP_E;   //!
   TBranch        *b_C_TRUEP_X;   //!
   TBranch        *b_C_TRUEP_Y;   //!
   TBranch        *b_C_TRUEP_Z;   //!
   TBranch        *b_C_TRUEPT;   //!
   TBranch        *b_C_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_C_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_C_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_C_TRUEENDVERTEX_X;   //!
   TBranch        *b_C_TRUEENDVERTEX_Y;   //!
   TBranch        *b_C_TRUEENDVERTEX_Z;   //!
   TBranch        *b_C_TRUEISSTABLE;   //!
   TBranch        *b_C_TRUETAU;   //!
   TBranch        *b_C_OSCIL;   //!
   TBranch        *b_Lc_nPhotos;   //!
   TBranch        *b_Lc_MC_MOTHER_ID;   //!
   TBranch        *b_Lc_MC_MOTHER_KEY;   //!
   TBranch        *b_Lc_MC_GD_MOTHER_ID;   //!
   TBranch        *b_Lc_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_Lc_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_Lc_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_Lc_TRUEP_E;   //!
   TBranch        *b_Lc_TRUEP_X;   //!
   TBranch        *b_Lc_TRUEP_Y;   //!
   TBranch        *b_Lc_TRUEP_Z;   //!
   TBranch        *b_Lc_TRUEPT;   //!
   TBranch        *b_Lc_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Lc_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Lc_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Lc_TRUEENDVERTEX_X;   //!
   TBranch        *b_Lc_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Lc_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Lc_TRUEISSTABLE;   //!
   TBranch        *b_Lc_TRUETAU;   //!
   TBranch        *b_Lc_OSCIL;   //!
   TBranch        *b_LcP_nPhotos;   //!
   TBranch        *b_LcP_MC_MOTHER_ID;   //!
   TBranch        *b_LcP_MC_MOTHER_KEY;   //!
   TBranch        *b_LcP_MC_GD_MOTHER_ID;   //!
   TBranch        *b_LcP_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_LcP_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_LcP_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_LcP_TRUEP_E;   //!
   TBranch        *b_LcP_TRUEP_X;   //!
   TBranch        *b_LcP_TRUEP_Y;   //!
   TBranch        *b_LcP_TRUEP_Z;   //!
   TBranch        *b_LcP_TRUEPT;   //!
   TBranch        *b_LcP_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_LcP_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_LcP_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_LcP_TRUEENDVERTEX_X;   //!
   TBranch        *b_LcP_TRUEENDVERTEX_Y;   //!
   TBranch        *b_LcP_TRUEENDVERTEX_Z;   //!
   TBranch        *b_LcP_TRUEISSTABLE;   //!
   TBranch        *b_LcP_TRUETAU;   //!
   TBranch        *b_LcP_OSCIL;   //!
   TBranch        *b_LcK_nPhotos;   //!
   TBranch        *b_LcK_MC_MOTHER_ID;   //!
   TBranch        *b_LcK_MC_MOTHER_KEY;   //!
   TBranch        *b_LcK_MC_GD_MOTHER_ID;   //!
   TBranch        *b_LcK_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_LcK_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_LcK_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_LcK_TRUEP_E;   //!
   TBranch        *b_LcK_TRUEP_X;   //!
   TBranch        *b_LcK_TRUEP_Y;   //!
   TBranch        *b_LcK_TRUEP_Z;   //!
   TBranch        *b_LcK_TRUEPT;   //!
   TBranch        *b_LcK_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_LcK_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_LcK_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_LcK_TRUEENDVERTEX_X;   //!
   TBranch        *b_LcK_TRUEENDVERTEX_Y;   //!
   TBranch        *b_LcK_TRUEENDVERTEX_Z;   //!
   TBranch        *b_LcK_TRUEISSTABLE;   //!
   TBranch        *b_LcK_TRUETAU;   //!
   TBranch        *b_LcK_OSCIL;   //!
   TBranch        *b_LcPi_nPhotos;   //!
   TBranch        *b_LcPi_MC_MOTHER_ID;   //!
   TBranch        *b_LcPi_MC_MOTHER_KEY;   //!
   TBranch        *b_LcPi_MC_GD_MOTHER_ID;   //!
   TBranch        *b_LcPi_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_LcPi_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_LcPi_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_LcPi_TRUEP_E;   //!
   TBranch        *b_LcPi_TRUEP_X;   //!
   TBranch        *b_LcPi_TRUEP_Y;   //!
   TBranch        *b_LcPi_TRUEP_Z;   //!
   TBranch        *b_LcPi_TRUEPT;   //!
   TBranch        *b_LcPi_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_LcPi_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_LcPi_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_LcPi_TRUEENDVERTEX_X;   //!
   TBranch        *b_LcPi_TRUEENDVERTEX_Y;   //!
   TBranch        *b_LcPi_TRUEENDVERTEX_Z;   //!
   TBranch        *b_LcPi_TRUEISSTABLE;   //!
   TBranch        *b_LcPi_TRUETAU;   //!
   TBranch        *b_LcPi_OSCIL;   //!
   TBranch        *b_PiP_nPhotos;   //!
   TBranch        *b_PiP_MC_MOTHER_ID;   //!
   TBranch        *b_PiP_MC_MOTHER_KEY;   //!
   TBranch        *b_PiP_MC_GD_MOTHER_ID;   //!
   TBranch        *b_PiP_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_PiP_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_PiP_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_PiP_TRUEP_E;   //!
   TBranch        *b_PiP_TRUEP_X;   //!
   TBranch        *b_PiP_TRUEP_Y;   //!
   TBranch        *b_PiP_TRUEP_Z;   //!
   TBranch        *b_PiP_TRUEPT;   //!
   TBranch        *b_PiP_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_PiP_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_PiP_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_PiP_TRUEENDVERTEX_X;   //!
   TBranch        *b_PiP_TRUEENDVERTEX_Y;   //!
   TBranch        *b_PiP_TRUEENDVERTEX_Z;   //!
   TBranch        *b_PiP_TRUEISSTABLE;   //!
   TBranch        *b_PiP_TRUETAU;   //!
   TBranch        *b_PiP_OSCIL;   //!
   TBranch        *b_KM_nPhotos;   //!
   TBranch        *b_KM_MC_MOTHER_ID;   //!
   TBranch        *b_KM_MC_MOTHER_KEY;   //!
   TBranch        *b_KM_MC_GD_MOTHER_ID;   //!
   TBranch        *b_KM_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_KM_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_KM_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_KM_TRUEP_E;   //!
   TBranch        *b_KM_TRUEP_X;   //!
   TBranch        *b_KM_TRUEP_Y;   //!
   TBranch        *b_KM_TRUEP_Z;   //!
   TBranch        *b_KM_TRUEPT;   //!
   TBranch        *b_KM_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_KM_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_KM_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_KM_TRUEENDVERTEX_X;   //!
   TBranch        *b_KM_TRUEENDVERTEX_Y;   //!
   TBranch        *b_KM_TRUEENDVERTEX_Z;   //!
   TBranch        *b_KM_TRUEISSTABLE;   //!
   TBranch        *b_KM_TRUETAU;   //!
   TBranch        *b_KM_OSCIL;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!

   MCDecayTree(TTree *tree=0);
   virtual ~MCDecayTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MCDecayTree_cxx
MCDecayTree::MCDecayTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("3638.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("3638.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("3638.root:/MCDecayTreeTuple");
      dir->GetObject("MCDecayTree",tree);

   }
   Init(tree);
}

MCDecayTree::~MCDecayTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MCDecayTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MCDecayTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MCDecayTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("C_nPhotos", &C_nPhotos, &b_C_nPhotos);
   fChain->SetBranchAddress("C_MC_MOTHER_ID", &C_MC_MOTHER_ID, &b_C_MC_MOTHER_ID);
   fChain->SetBranchAddress("C_MC_MOTHER_KEY", &C_MC_MOTHER_KEY, &b_C_MC_MOTHER_KEY);
   fChain->SetBranchAddress("C_MC_GD_MOTHER_ID", &C_MC_GD_MOTHER_ID, &b_C_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("C_MC_GD_MOTHER_KEY", &C_MC_GD_MOTHER_KEY, &b_C_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("C_MC_GD_GD_MOTHER_ID", &C_MC_GD_GD_MOTHER_ID, &b_C_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("C_MC_GD_GD_MOTHER_KEY", &C_MC_GD_GD_MOTHER_KEY, &b_C_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("C_TRUEP_E", &C_TRUEP_E, &b_C_TRUEP_E);
   fChain->SetBranchAddress("C_TRUEP_X", &C_TRUEP_X, &b_C_TRUEP_X);
   fChain->SetBranchAddress("C_TRUEP_Y", &C_TRUEP_Y, &b_C_TRUEP_Y);
   fChain->SetBranchAddress("C_TRUEP_Z", &C_TRUEP_Z, &b_C_TRUEP_Z);
   fChain->SetBranchAddress("C_TRUEPT", &C_TRUEPT, &b_C_TRUEPT);
   fChain->SetBranchAddress("C_TRUEORIGINVERTEX_X", &C_TRUEORIGINVERTEX_X, &b_C_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("C_TRUEORIGINVERTEX_Y", &C_TRUEORIGINVERTEX_Y, &b_C_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("C_TRUEORIGINVERTEX_Z", &C_TRUEORIGINVERTEX_Z, &b_C_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("C_TRUEENDVERTEX_X", &C_TRUEENDVERTEX_X, &b_C_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("C_TRUEENDVERTEX_Y", &C_TRUEENDVERTEX_Y, &b_C_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("C_TRUEENDVERTEX_Z", &C_TRUEENDVERTEX_Z, &b_C_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("C_TRUEISSTABLE", &C_TRUEISSTABLE, &b_C_TRUEISSTABLE);
   fChain->SetBranchAddress("C_TRUETAU", &C_TRUETAU, &b_C_TRUETAU);
   fChain->SetBranchAddress("C_OSCIL", &C_OSCIL, &b_C_OSCIL);
   fChain->SetBranchAddress("Lc_nPhotos", &Lc_nPhotos, &b_Lc_nPhotos);
   fChain->SetBranchAddress("Lc_MC_MOTHER_ID", &Lc_MC_MOTHER_ID, &b_Lc_MC_MOTHER_ID);
   fChain->SetBranchAddress("Lc_MC_MOTHER_KEY", &Lc_MC_MOTHER_KEY, &b_Lc_MC_MOTHER_KEY);
   fChain->SetBranchAddress("Lc_MC_GD_MOTHER_ID", &Lc_MC_GD_MOTHER_ID, &b_Lc_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("Lc_MC_GD_MOTHER_KEY", &Lc_MC_GD_MOTHER_KEY, &b_Lc_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("Lc_MC_GD_GD_MOTHER_ID", &Lc_MC_GD_GD_MOTHER_ID, &b_Lc_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("Lc_MC_GD_GD_MOTHER_KEY", &Lc_MC_GD_GD_MOTHER_KEY, &b_Lc_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("Lc_TRUEP_E", &Lc_TRUEP_E, &b_Lc_TRUEP_E);
   fChain->SetBranchAddress("Lc_TRUEP_X", &Lc_TRUEP_X, &b_Lc_TRUEP_X);
   fChain->SetBranchAddress("Lc_TRUEP_Y", &Lc_TRUEP_Y, &b_Lc_TRUEP_Y);
   fChain->SetBranchAddress("Lc_TRUEP_Z", &Lc_TRUEP_Z, &b_Lc_TRUEP_Z);
   fChain->SetBranchAddress("Lc_TRUEPT", &Lc_TRUEPT, &b_Lc_TRUEPT);
   fChain->SetBranchAddress("Lc_TRUEORIGINVERTEX_X", &Lc_TRUEORIGINVERTEX_X, &b_Lc_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Lc_TRUEORIGINVERTEX_Y", &Lc_TRUEORIGINVERTEX_Y, &b_Lc_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Lc_TRUEORIGINVERTEX_Z", &Lc_TRUEORIGINVERTEX_Z, &b_Lc_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Lc_TRUEENDVERTEX_X", &Lc_TRUEENDVERTEX_X, &b_Lc_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Lc_TRUEENDVERTEX_Y", &Lc_TRUEENDVERTEX_Y, &b_Lc_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Lc_TRUEENDVERTEX_Z", &Lc_TRUEENDVERTEX_Z, &b_Lc_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Lc_TRUEISSTABLE", &Lc_TRUEISSTABLE, &b_Lc_TRUEISSTABLE);
   fChain->SetBranchAddress("Lc_TRUETAU", &Lc_TRUETAU, &b_Lc_TRUETAU);
   fChain->SetBranchAddress("Lc_OSCIL", &Lc_OSCIL, &b_Lc_OSCIL);
   fChain->SetBranchAddress("LcP_nPhotos", &LcP_nPhotos, &b_LcP_nPhotos);
   fChain->SetBranchAddress("LcP_MC_MOTHER_ID", &LcP_MC_MOTHER_ID, &b_LcP_MC_MOTHER_ID);
   fChain->SetBranchAddress("LcP_MC_MOTHER_KEY", &LcP_MC_MOTHER_KEY, &b_LcP_MC_MOTHER_KEY);
   fChain->SetBranchAddress("LcP_MC_GD_MOTHER_ID", &LcP_MC_GD_MOTHER_ID, &b_LcP_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcP_MC_GD_MOTHER_KEY", &LcP_MC_GD_MOTHER_KEY, &b_LcP_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcP_MC_GD_GD_MOTHER_ID", &LcP_MC_GD_GD_MOTHER_ID, &b_LcP_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcP_MC_GD_GD_MOTHER_KEY", &LcP_MC_GD_GD_MOTHER_KEY, &b_LcP_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcP_TRUEP_E", &LcP_TRUEP_E, &b_LcP_TRUEP_E);
   fChain->SetBranchAddress("LcP_TRUEP_X", &LcP_TRUEP_X, &b_LcP_TRUEP_X);
   fChain->SetBranchAddress("LcP_TRUEP_Y", &LcP_TRUEP_Y, &b_LcP_TRUEP_Y);
   fChain->SetBranchAddress("LcP_TRUEP_Z", &LcP_TRUEP_Z, &b_LcP_TRUEP_Z);
   fChain->SetBranchAddress("LcP_TRUEPT", &LcP_TRUEPT, &b_LcP_TRUEPT);
   fChain->SetBranchAddress("LcP_TRUEORIGINVERTEX_X", &LcP_TRUEORIGINVERTEX_X, &b_LcP_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("LcP_TRUEORIGINVERTEX_Y", &LcP_TRUEORIGINVERTEX_Y, &b_LcP_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("LcP_TRUEORIGINVERTEX_Z", &LcP_TRUEORIGINVERTEX_Z, &b_LcP_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("LcP_TRUEENDVERTEX_X", &LcP_TRUEENDVERTEX_X, &b_LcP_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("LcP_TRUEENDVERTEX_Y", &LcP_TRUEENDVERTEX_Y, &b_LcP_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("LcP_TRUEENDVERTEX_Z", &LcP_TRUEENDVERTEX_Z, &b_LcP_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("LcP_TRUEISSTABLE", &LcP_TRUEISSTABLE, &b_LcP_TRUEISSTABLE);
   fChain->SetBranchAddress("LcP_TRUETAU", &LcP_TRUETAU, &b_LcP_TRUETAU);
   fChain->SetBranchAddress("LcP_OSCIL", &LcP_OSCIL, &b_LcP_OSCIL);
   fChain->SetBranchAddress("LcK_nPhotos", &LcK_nPhotos, &b_LcK_nPhotos);
   fChain->SetBranchAddress("LcK_MC_MOTHER_ID", &LcK_MC_MOTHER_ID, &b_LcK_MC_MOTHER_ID);
   fChain->SetBranchAddress("LcK_MC_MOTHER_KEY", &LcK_MC_MOTHER_KEY, &b_LcK_MC_MOTHER_KEY);
   fChain->SetBranchAddress("LcK_MC_GD_MOTHER_ID", &LcK_MC_GD_MOTHER_ID, &b_LcK_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcK_MC_GD_MOTHER_KEY", &LcK_MC_GD_MOTHER_KEY, &b_LcK_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcK_MC_GD_GD_MOTHER_ID", &LcK_MC_GD_GD_MOTHER_ID, &b_LcK_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcK_MC_GD_GD_MOTHER_KEY", &LcK_MC_GD_GD_MOTHER_KEY, &b_LcK_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcK_TRUEP_E", &LcK_TRUEP_E, &b_LcK_TRUEP_E);
   fChain->SetBranchAddress("LcK_TRUEP_X", &LcK_TRUEP_X, &b_LcK_TRUEP_X);
   fChain->SetBranchAddress("LcK_TRUEP_Y", &LcK_TRUEP_Y, &b_LcK_TRUEP_Y);
   fChain->SetBranchAddress("LcK_TRUEP_Z", &LcK_TRUEP_Z, &b_LcK_TRUEP_Z);
   fChain->SetBranchAddress("LcK_TRUEPT", &LcK_TRUEPT, &b_LcK_TRUEPT);
   fChain->SetBranchAddress("LcK_TRUEORIGINVERTEX_X", &LcK_TRUEORIGINVERTEX_X, &b_LcK_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("LcK_TRUEORIGINVERTEX_Y", &LcK_TRUEORIGINVERTEX_Y, &b_LcK_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("LcK_TRUEORIGINVERTEX_Z", &LcK_TRUEORIGINVERTEX_Z, &b_LcK_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("LcK_TRUEENDVERTEX_X", &LcK_TRUEENDVERTEX_X, &b_LcK_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("LcK_TRUEENDVERTEX_Y", &LcK_TRUEENDVERTEX_Y, &b_LcK_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("LcK_TRUEENDVERTEX_Z", &LcK_TRUEENDVERTEX_Z, &b_LcK_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("LcK_TRUEISSTABLE", &LcK_TRUEISSTABLE, &b_LcK_TRUEISSTABLE);
   fChain->SetBranchAddress("LcK_TRUETAU", &LcK_TRUETAU, &b_LcK_TRUETAU);
   fChain->SetBranchAddress("LcK_OSCIL", &LcK_OSCIL, &b_LcK_OSCIL);
   fChain->SetBranchAddress("LcPi_nPhotos", &LcPi_nPhotos, &b_LcPi_nPhotos);
   fChain->SetBranchAddress("LcPi_MC_MOTHER_ID", &LcPi_MC_MOTHER_ID, &b_LcPi_MC_MOTHER_ID);
   fChain->SetBranchAddress("LcPi_MC_MOTHER_KEY", &LcPi_MC_MOTHER_KEY, &b_LcPi_MC_MOTHER_KEY);
   fChain->SetBranchAddress("LcPi_MC_GD_MOTHER_ID", &LcPi_MC_GD_MOTHER_ID, &b_LcPi_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcPi_MC_GD_MOTHER_KEY", &LcPi_MC_GD_MOTHER_KEY, &b_LcPi_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcPi_MC_GD_GD_MOTHER_ID", &LcPi_MC_GD_GD_MOTHER_ID, &b_LcPi_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("LcPi_MC_GD_GD_MOTHER_KEY", &LcPi_MC_GD_GD_MOTHER_KEY, &b_LcPi_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("LcPi_TRUEP_E", &LcPi_TRUEP_E, &b_LcPi_TRUEP_E);
   fChain->SetBranchAddress("LcPi_TRUEP_X", &LcPi_TRUEP_X, &b_LcPi_TRUEP_X);
   fChain->SetBranchAddress("LcPi_TRUEP_Y", &LcPi_TRUEP_Y, &b_LcPi_TRUEP_Y);
   fChain->SetBranchAddress("LcPi_TRUEP_Z", &LcPi_TRUEP_Z, &b_LcPi_TRUEP_Z);
   fChain->SetBranchAddress("LcPi_TRUEPT", &LcPi_TRUEPT, &b_LcPi_TRUEPT);
   fChain->SetBranchAddress("LcPi_TRUEORIGINVERTEX_X", &LcPi_TRUEORIGINVERTEX_X, &b_LcPi_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("LcPi_TRUEORIGINVERTEX_Y", &LcPi_TRUEORIGINVERTEX_Y, &b_LcPi_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("LcPi_TRUEORIGINVERTEX_Z", &LcPi_TRUEORIGINVERTEX_Z, &b_LcPi_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("LcPi_TRUEENDVERTEX_X", &LcPi_TRUEENDVERTEX_X, &b_LcPi_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("LcPi_TRUEENDVERTEX_Y", &LcPi_TRUEENDVERTEX_Y, &b_LcPi_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("LcPi_TRUEENDVERTEX_Z", &LcPi_TRUEENDVERTEX_Z, &b_LcPi_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("LcPi_TRUEISSTABLE", &LcPi_TRUEISSTABLE, &b_LcPi_TRUEISSTABLE);
   fChain->SetBranchAddress("LcPi_TRUETAU", &LcPi_TRUETAU, &b_LcPi_TRUETAU);
   fChain->SetBranchAddress("LcPi_OSCIL", &LcPi_OSCIL, &b_LcPi_OSCIL);
   fChain->SetBranchAddress("PiP_nPhotos", &PiP_nPhotos, &b_PiP_nPhotos);
   fChain->SetBranchAddress("PiP_MC_MOTHER_ID", &PiP_MC_MOTHER_ID, &b_PiP_MC_MOTHER_ID);
   fChain->SetBranchAddress("PiP_MC_MOTHER_KEY", &PiP_MC_MOTHER_KEY, &b_PiP_MC_MOTHER_KEY);
   fChain->SetBranchAddress("PiP_MC_GD_MOTHER_ID", &PiP_MC_GD_MOTHER_ID, &b_PiP_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("PiP_MC_GD_MOTHER_KEY", &PiP_MC_GD_MOTHER_KEY, &b_PiP_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("PiP_MC_GD_GD_MOTHER_ID", &PiP_MC_GD_GD_MOTHER_ID, &b_PiP_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("PiP_MC_GD_GD_MOTHER_KEY", &PiP_MC_GD_GD_MOTHER_KEY, &b_PiP_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("PiP_TRUEP_E", &PiP_TRUEP_E, &b_PiP_TRUEP_E);
   fChain->SetBranchAddress("PiP_TRUEP_X", &PiP_TRUEP_X, &b_PiP_TRUEP_X);
   fChain->SetBranchAddress("PiP_TRUEP_Y", &PiP_TRUEP_Y, &b_PiP_TRUEP_Y);
   fChain->SetBranchAddress("PiP_TRUEP_Z", &PiP_TRUEP_Z, &b_PiP_TRUEP_Z);
   fChain->SetBranchAddress("PiP_TRUEPT", &PiP_TRUEPT, &b_PiP_TRUEPT);
   fChain->SetBranchAddress("PiP_TRUEORIGINVERTEX_X", &PiP_TRUEORIGINVERTEX_X, &b_PiP_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("PiP_TRUEORIGINVERTEX_Y", &PiP_TRUEORIGINVERTEX_Y, &b_PiP_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("PiP_TRUEORIGINVERTEX_Z", &PiP_TRUEORIGINVERTEX_Z, &b_PiP_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("PiP_TRUEENDVERTEX_X", &PiP_TRUEENDVERTEX_X, &b_PiP_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("PiP_TRUEENDVERTEX_Y", &PiP_TRUEENDVERTEX_Y, &b_PiP_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("PiP_TRUEENDVERTEX_Z", &PiP_TRUEENDVERTEX_Z, &b_PiP_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("PiP_TRUEISSTABLE", &PiP_TRUEISSTABLE, &b_PiP_TRUEISSTABLE);
   fChain->SetBranchAddress("PiP_TRUETAU", &PiP_TRUETAU, &b_PiP_TRUETAU);
   fChain->SetBranchAddress("PiP_OSCIL", &PiP_OSCIL, &b_PiP_OSCIL);
   fChain->SetBranchAddress("KM_nPhotos", &KM_nPhotos, &b_KM_nPhotos);
   fChain->SetBranchAddress("KM_MC_MOTHER_ID", &KM_MC_MOTHER_ID, &b_KM_MC_MOTHER_ID);
   fChain->SetBranchAddress("KM_MC_MOTHER_KEY", &KM_MC_MOTHER_KEY, &b_KM_MC_MOTHER_KEY);
   fChain->SetBranchAddress("KM_MC_GD_MOTHER_ID", &KM_MC_GD_MOTHER_ID, &b_KM_MC_GD_MOTHER_ID);
   fChain->SetBranchAddress("KM_MC_GD_MOTHER_KEY", &KM_MC_GD_MOTHER_KEY, &b_KM_MC_GD_MOTHER_KEY);
   fChain->SetBranchAddress("KM_MC_GD_GD_MOTHER_ID", &KM_MC_GD_GD_MOTHER_ID, &b_KM_MC_GD_GD_MOTHER_ID);
   fChain->SetBranchAddress("KM_MC_GD_GD_MOTHER_KEY", &KM_MC_GD_GD_MOTHER_KEY, &b_KM_MC_GD_GD_MOTHER_KEY);
   fChain->SetBranchAddress("KM_TRUEP_E", &KM_TRUEP_E, &b_KM_TRUEP_E);
   fChain->SetBranchAddress("KM_TRUEP_X", &KM_TRUEP_X, &b_KM_TRUEP_X);
   fChain->SetBranchAddress("KM_TRUEP_Y", &KM_TRUEP_Y, &b_KM_TRUEP_Y);
   fChain->SetBranchAddress("KM_TRUEP_Z", &KM_TRUEP_Z, &b_KM_TRUEP_Z);
   fChain->SetBranchAddress("KM_TRUEPT", &KM_TRUEPT, &b_KM_TRUEPT);
   fChain->SetBranchAddress("KM_TRUEORIGINVERTEX_X", &KM_TRUEORIGINVERTEX_X, &b_KM_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("KM_TRUEORIGINVERTEX_Y", &KM_TRUEORIGINVERTEX_Y, &b_KM_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("KM_TRUEORIGINVERTEX_Z", &KM_TRUEORIGINVERTEX_Z, &b_KM_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("KM_TRUEENDVERTEX_X", &KM_TRUEENDVERTEX_X, &b_KM_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("KM_TRUEENDVERTEX_Y", &KM_TRUEENDVERTEX_Y, &b_KM_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("KM_TRUEENDVERTEX_Z", &KM_TRUEENDVERTEX_Z, &b_KM_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("KM_TRUEISSTABLE", &KM_TRUEISSTABLE, &b_KM_TRUEISSTABLE);
   fChain->SetBranchAddress("KM_TRUETAU", &KM_TRUETAU, &b_KM_TRUETAU);
   fChain->SetBranchAddress("KM_OSCIL", &KM_OSCIL, &b_KM_OSCIL);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   Notify();
}

Bool_t MCDecayTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MCDecayTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MCDecayTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MCDecayTree_cxx
