# Mass hypotheses
1. Already prepared three projects of different mass hypotheses, the nominal one is 3738 (for Omegacc)
2. cd 3838/ (different mass), the command lines are in the gen.sh, where all python files are the option for the generation
3. 26165856.py, the option file of the decayfile named by the EventType, line 30 perfomed the the changing of the mass with 3.838 GeV. More information of the decayfile please see https://lhcb.github.io/starterkit-lessons/second-analysis-steps/simulation-dkfiles.html
4. The other process are exactly same with the Acceptance efficiency
