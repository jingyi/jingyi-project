MyPreambulo = [
    'from LoKiPhysMC.decorators import *',
    'from LoKiPhysMC.functions import mcMatch' ,
    'from LoKiCore.functions import monitor'
]

def applyMVA( name, 
              SelB,
              MVAVars,
              MVAxmlFile,
              MVACutValue
              ):
    from MVADictHelpers import addTMVAclassifierValue
    from Configurables import FilterDesktop as MVAFilterDesktop
    
    _FilterB = MVAFilterDesktop( name + "Filter",
                                 Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )
    
    addTMVAclassifierValue( Component = _FilterB,
                            XMLFile   = MVAxmlFile,
                            Variables = MVAVars,
                            ToolName  = name )

    from PhysSelPython.Wrappers import Selection
    return Selection( name,
                      Algorithm =  _FilterB,
                      RequiredSelections = [ SelB ] )


def fillTuple( tuple, myTriggerList ):

    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolANNPID",           # Different tuning...
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolL0Data",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolPropertime" ,
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolPrimaries
    tuple.addTool(TupleToolPrimaries)
    tuple.TupleToolPrimaries.InputLocation= "/Event/Turbo/Primary"

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Lc')
    tuple.addTool(TupleToolDecay, name = 'C')
    
    # TISTOS for Lc
    tuple.Lc.ToolList+=[ "TupleToolTISTOS" ]
    tuple.Lc.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Lc.TupleToolTISTOS.Verbose=True
    tuple.Lc.TupleToolTISTOS.TriggerList = myTriggerList

    tuple.C.ToolList+=[ "TupleToolTISTOS" ]
    tuple.C.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.C.TupleToolTISTOS.Verbose=True
    tuple.C.TupleToolTISTOS.TriggerList = myTriggerList

    # DecayTree 
    from Configurables import TupleToolDecayTreeFitter
    tuple.C.ToolList +=  [ "TupleToolDecayTreeFitter/PVFit",          # fit with PV constraint
                           "TupleToolDecayTreeFitter/MassFit",        # fit with J/psi mass constraint
                           "TupleToolDecayTreeFitter/FullFit"         # fit with both
                           ]       

    tuple.C.addTool(TupleToolDecayTreeFitter("PVFit"))
    tuple.C.PVFit.Verbose = True
    tuple.C.PVFit.constrainToOriginVertex = True
    
    tuple.C.addTool(TupleToolDecayTreeFitter("MassFit"))
    tuple.C.MassFit.constrainToOriginVertex = False
    tuple.C.MassFit.daughtersToConstrain = [ "Xi_c+" ]
    
    tuple.C.addTool(TupleToolDecayTreeFitter("FullFit"))
    tuple.C.FullFit.Verbose = True
    tuple.C.FullFit.constrainToOriginVertex = True
    tuple.C.FullFit.daughtersToConstrain = [ "Xi_c+" ]

    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)


    LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
    LoKi_B.Variables = {
            "LOKI_FDCHI2"          : "BPVVDCHI2",
            "LOKI_FDS"             : "BPVDLS",
            "LOKI_DIRA"            : "BPVDIRA",
            "LV01"                 : "LV01",
            "LV02"                 : "LV02",
            "DTF_CHI2NDOF"         : "DTF_CHI2NDOF (     True , strings ( [ 'Xi_c+' ] ) )", 
            "DTF_M"                : "DTF_FUN      ( M , True , strings ( [ 'Xi_c+' ] ) )",
            "DTF_CTAU"             : "DTF_CTAU     ( 0 , True , strings ( [ 'Xi_c+' ] ) )",            
            "DTF_CTAUERR"          : "DTF_CTAUERR  ( 0 , True , strings ( [ 'Xi_c+' ] ) )",
            "DTF_CTAUS"            : "DTF_CTAUSIGNIFICANCE ( 0 , True , strings ( [ 'Xi_c+' ] ) )", 
            "Lc_DTF_CTAU"          : "DTF_CTAU     ( 'Xi_c+' == ABSID  , True , strings ( [ 'Xi_c+' ] ) )",
            "Lc_DTF_CTAUERR"       : "DTF_CTAUERR  ( 'Xi_c+' == ABSID  , True , strings ( [ 'Xi_c+' ] ) )",
            "Lc_DTF_CTAUS"         : "DTF_CTAUSIGNIFICANCE ( 'Xi_c+' == ABSID  , True , strings ( [ 'Xi_c+' ] ) )",
            "DTF_DM"               : "(DTF_FUN( M , True ) - DTF_FUN( CHILD(1,M) , True ))",
            "DTF_PV_CTAU"          : "DTF_CTAU     ( 0 , True )",            
            "DTF_PV_CTAUERR"       : "DTF_CTAUERR  ( 0 , True )",            
            "SumXicIPCHI2"         : "( CHILD(MIPCHI2DV(), 1, 1) + CHILD(MIPCHI2DV(), 1, 2) + CHILD(MIPCHI2DV(), 1, 3) )",
            "SumOmegaccIPCHI2"        : "( CHILD(MIPCHI2DV(), 1, 1) + CHILD(MIPCHI2DV(), 1, 2) + CHILD(MIPCHI2DV(), 1, 3)  + CHILD(MIPCHI2DV(), 2) +  CHILD(MIPCHI2DV(), 3)  )",
            "SumXicPT"             : "( CHILD(PT, 1, 1) + CHILD(PT, 1, 2) + CHILD(PT, 1, 3) )",
            "SumOmegaccPT"            : "( CHILD(PT, 1, 1) + CHILD(PT, 1, 2) + CHILD(PT, 1, 3) + CHILD(PT, 2) + CHILD(PT, 3) )",
            "DTF_PV_CHI2"          : "DTF_CHI2(True)"
            }
    
    tuple.C.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]
    tuple.C.addTool(LoKi_B)

    """
    MC information
    """
    from Configurables import TupleToolMCTruth
    tuple.ToolList += [
        "TupleToolMCBackgroundInfo",
        "TupleToolMCTruth"
        ]

    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  [
        "MCTupleToolAngles"
        , "MCTupleToolHierarchy"
        , "MCTupleToolKinematic"
        ]
    tuple.addTool(MCTruth)
    

## (0) setting the year of the data
year = '2016'

## (2) get pions form PersistReco 
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllNoPIDsKaons as Kaons
from StandardParticles import StdAllNoPIDsProtons as Protons

## This is the important lines ... 
from PhysConf.Selections import RebuildSelection
Pions = RebuildSelection(Pions)
Kaons = RebuildSelection(Kaons)
Protons =  RebuildSelection(Protons)

## (3) create Xic, Omegacc candidates 
from PhysConf.Selections import CombineSelection
Xic = CombineSelection (
    'Xic'       , ## the name 
    [ Pions, Kaons, Protons ] , ## input
    DecayDescriptor = '[ Xi_c+ -> K- p+  pi+ ]cc',
    Preambulo       = MyPreambulo  ,
    CombinationCut  = "AALL", 
    MotherCut       = "mcMatch('[ Xi_c+ ==> K- p+  pi+  ]CC' )", 
    CheckOverlapTool = "LoKi::CheckOverlap",
    DaughtersCuts = {
                "pi+" :  "mcMatch('[Xi_c+ ==> K-  p+ ^pi+ ]CC')",
                "p+"  :  "mcMatch('[Xi_c+ ==> K- ^p+  pi+ ]CC')",
                "K-"  :  "mcMatch('[Xi_c+ ==>^K-  p+  pi+ ]CC')",
                }
    )


Omegacc = CombineSelection (
    'Omegacc'       , ## the name 
    [ Xic , Kaons, Pions ] , ## input
    DecayDescriptor = '[Omega_cc+ ->  Xi_c+ pi+ K-]cc',
    Preambulo       = MyPreambulo  ,
    CombinationCut  = "AALL", 
    MotherCut       = "mcMatch('[Omega_cc+ => Xi_c+ pi+ K-]CC')", 
    CheckOverlapTool = "LoKi::CheckOverlap" ,
    DaughtersCuts = {
                "pi+" :  "mcMatch('[Omega_cc+ => Xi_c+ ^pi+ K-]CC')"
             ,  "K-"  :  "mcMatch('[Omega_cc+ => Xi_c+ pi+ ^K-]CC')"
             }    
)

## (5) fill tuple
myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    # L0 Muon
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    
    # Hlt1 track    
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",

    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision"
    ]


from PhysConf.Selections import TupleSelection 
Omegacc_Tuple = TupleSelection (
    'OmegaccTuple'      ,
    [ Omegacc ] , 
    Decay = "[ Omega_cc+ ->^( Xi_c+ ->^p+ ^K- ^pi+ ) ^pi+ ^K-]CC",
    Branches = {
    "LcP"    :  "[ Omega_cc+ -> ( Xi_c+ ->^p+  K-  pi+ ) pi+ K- ]CC",
    "LcK"    :  "[ Omega_cc+ -> ( Xi_c+ -> p+ ^K-  pi+ ) pi+ K- ]CC",
    "LcPi"   :  "[ Omega_cc+ -> ( Xi_c+ -> p+  K- ^pi+ ) pi+ K- ]CC",
    "Lc"     :  "[ Omega_cc+ ->^( Xi_c+ -> p+  K-  pi+ ) pi+ K- ]CC",
    "PiP"    :  "[ Omega_cc+ -> ( Xi_c+ -> p+  K-  pi+ )^pi+ K- ]CC",
    "KM"    :  "[ Omega_cc+ -> ( Xi_c+ -> p+  K-  pi+ ) pi+ ^K- ]CC",
    "C"      :"^([ Omega_cc+ -> ( Xi_c+ -> p+  K-  pi+ ) pi+ K- ]CC)"
    } 
)

tuple = Omegacc_Tuple.algorithm() 
fillTuple( tuple,
           myTriggerList)

## MCDecayTree
##from Configurables import TupleToolDecayTreeFitter
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

mcdtt = MCDecayTreeTuple("MCDecayTreeTuple")
mcdtt.Decay = "[ Omega_cc+ =>^( Xi_c+ ==>^p+ ^K- ^pi+ ) ^pi+ ^K-]CC"
mcdtt.addBranches( 
{
"LcP"    :  "[ Omega_cc+ => ( Xi_c+ ==>^p+  K-  pi+ ) pi+ K- ]CC",
"LcK"    :  "[ Omega_cc+ => ( Xi_c+ ==> p+ ^K-  pi+ ) pi+ K- ]CC",
"LcPi"   :  "[ Omega_cc+ => ( Xi_c+ ==> p+  K- ^pi+ ) pi+ K- ]CC",
"Lc"     :  "[ Omega_cc+ =>^( Xi_c+ ==> p+  K-  pi+ ) pi+ K- ]CC",
"PiP"    :  "[ Omega_cc+ => ( Xi_c+ ==> p+  K-  pi+ )^pi+ K- ]CC",
"KM"    :  "[ Omega_cc+ => ( Xi_c+ ==> p+  K-  pi+ ) pi+ ^K- ]CC",
"C"      :"^([ Omega_cc+ => ( Xi_c+ ==> p+  K-  pi+ ) pi+ K- ]CC)"
} 
)
mcdtt.ToolList += [
"MCTupleToolKinematic",
"MCTupleToolPrimaries",
"MCTupleToolAngles",
'MCTupleToolHierarchy',
"TupleToolRecoStats",
]

# # CheckPV
from Configurables import CheckPV
checkPV = CheckPV("checkPV")
checkPV.MinPVs = 1


## (6) build the final selection sequence

#from Configurables import GaudiSequencer 
#seq = GaudiSequencer("seq")
#seq.Members += [ checkPV,                            
#                Xic,
#                Omegacc,
#                Omegacc_Tuple ]


from PhysConf.Selections import SelectionSequence
ssq = SelectionSequence('ssq', Omegacc_Tuple)

from Configurables import GaudiSequencer
seq = GaudiSequencer("seq")
seq.Members += [checkPV,
                ssq.sequence()  ]


## (7) configure DaVinci
from Configurables import DaVinci 
dv = DaVinci(
    DataType   = year         ,
    InputType  = 'MDST'         , ## ATTENTION! 
    RootInTES  = "/Event/AllStreams" , ## ATTENTION! 
    ##
    Lumi       = False         , 
    Simulation = True          ,
    ## 
    TupleFile  = 'Tuple.root'   ,
    EvtMax     = -1
 )

## (8) insert our sequence into DaVinci 
dv.UserAlgorithms = [ mcdtt,seq ]
#seq.sequence() ] 



