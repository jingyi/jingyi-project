"""This file is an example of a user-defined binning scheme file, which """ \
"""can be passed as an argument to the multi-track calibration scripts.
The methods for constructing binning schema are defined in """ \
"""$PIDPERFSCRIPTSROOT/python/PIDPerfScripts/binning.py."""

from PIDPerfScripts.Binning import *
from PIDPerfScripts.Definitions import *

### DLL(K-pi), RICH (alternative schemes)


#Brunel_P = B(3000 : 9300 : 15600 : 19000 : 24400 : 29800 : 35200 : 40600 : 46000 : 51400 : 56800 : 62200 : 67600 : 73000 : 78400 : 83800 : 89200 : 94600 : 100000)
#------ Var name 'Brunel_ETA'------
#Brunel_ETA = B(1.5 : 2.375 : 3.25 : 4.125 : 5)
#------ Var name 'nTracks_Brunel'------
#nTracks_Brunel = B(0 : 50 : 200 : 300 : 500)

for trType in ('P','K','Pi'):
    ### for P/pi ID/misID performance plots

    # momentum
    AddBinScheme(trType, 'P', 'xicc', 2000, 150000)
    #AddUniformBins(trType, 'P', 'xicc', 20, 3000, 100000)
    AddBinBoundary(trType, 'P', 'xicc',5600)
    AddBinBoundary(trType, 'P', 'xicc',9200) 
    AddBinBoundary(trType, 'P', 'xicc',12800) 
    AddBinBoundary(trType, 'P', 'xicc',16400) 
    AddBinBoundary(trType, 'P', 'xicc',20000) 
    AddBinBoundary(trType, 'P', 'xicc',26000) 
    AddBinBoundary(trType, 'P', 'xicc',32000) 
    AddBinBoundary(trType, 'P', 'xicc',38000) 
    AddBinBoundary(trType, 'P', 'xicc',44000) 
    AddBinBoundary(trType, 'P', 'xicc',50000) 
    AddBinBoundary(trType, 'P', 'xicc',56000) 
    AddBinBoundary(trType, 'P', 'xicc',62000) 
    AddBinBoundary(trType, 'P', 'xicc',68000) 
    AddBinBoundary(trType, 'P', 'xicc',74000) 
    AddBinBoundary(trType, 'P', 'xicc',80000) 
    AddBinBoundary(trType, 'P', 'xicc',90000) 
    AddBinBoundary(trType, 'P', 'xicc',100000) 
    AddBinBoundary(trType, 'P', 'xicc',110000) 
    AddBinBoundary(trType, 'P', 'xicc',120000) 
    AddBinBoundary(trType, 'P', 'xicc',150000) 
    
    # momentum
    AddBinScheme(trType, 'Brunel_P', 'xicc', 3000, 100000)
    #AddUniformBins(trType, 'Brunel_P', 'xicc', 20, 3000, 100000)
    AddBinBoundary(trType, 'Brunel_P', 'xicc', 9500)
    AddBinBoundary(trType, 'Brunel_P', 'xicc',16000) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',21250) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',26500) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',31750) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',37000) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',42250) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',47500) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',52750) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',58000) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',63250) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',68500) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',73750) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',79000) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',84250) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',89500) 
    AddBinBoundary(trType, 'Brunel_P', 'xicc',94750) 



#Brunel_ETA = B(1.5 : 2.375 : 3.25 : 4.125 : 5)
    # eta
    AddBinScheme(trType, 'ETA', 'xicc', 1.5, 5)
    #AddUniformBins(trType, 'ETA', 'xicc', 5, 1.5, 5)
    AddBinBoundary(trType, 'ETA', 'xicc',2.25) 
    AddBinBoundary(trType, 'ETA', 'xicc',2.5) 
    AddBinBoundary(trType, 'ETA', 'xicc',2.75) 
    AddBinBoundary(trType, 'ETA', 'xicc',3.0) 
    AddBinBoundary(trType, 'ETA', 'xicc',3.25) 
    AddBinBoundary(trType, 'ETA', 'xicc',3.5) 
    AddBinBoundary(trType, 'ETA', 'xicc',3.75) 
    AddBinBoundary(trType, 'ETA', 'xicc',4.0) 
    AddBinBoundary(trType, 'ETA', 'xicc',4.33) 
    AddBinBoundary(trType, 'ETA', 'xicc',5.0) 
    
    # eta
    AddBinScheme(trType, 'Brunel_ETA', 'xicc', 1.5, 5)
    #AddUniformBins(trType, 'Brunel_ETA', 'xicc', 5, 1.5, 5)
    AddBinBoundary(trType, 'Brunel_ETA', 'xicc',2.4) 
    AddBinBoundary(trType, 'Brunel_ETA', 'xicc',3.3) 
    AddBinBoundary(trType, 'Brunel_ETA', 'xicc',4.2) 
 

    # nSPDHits
    AddBinScheme(trType, 'nSPDHits', 'xicc', 0, 1000)
    AddUniformBins(trType, 'nSPDHits', 'xicc', 5, 0, 1000)

#nTracks_Brunel = B(0 : 50 : 200 : 300 : 500)
    # nTracks
    AddBinScheme(trType, 'nTracks', 'xicc', 0, 500)
    #AddUniformBins(trType, 'nTracks', 'xicc', 5, 0, 500)
    AddBinBoundary(trType, 'nTracks', 'xicc',60) 
    AddBinBoundary(trType, 'nTracks', 'xicc',210) 
    AddBinBoundary(trType, 'nTracks', 'xicc',310) 
    
    # nTracks_Brunel
    AddBinScheme(trType, 'nTracks_Brunel', 'xicc', 0, 500)
    #AddUniformBins(trType, 'nTracks_Brunel', 'xicc', 5, 0, 500)
    AddBinBoundary(trType, 'nTracks_Brunel', 'xicc',60) 
    AddBinBoundary(trType, 'nTracks_Brunel', 'xicc',210) 
    AddBinBoundary(trType, 'nTracks_Brunel', 'xicc',310) 
 
