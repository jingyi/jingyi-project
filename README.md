# A guide for amateur to use git

# Prepare work
1. Visit https://gitlab.cern.ch and log in
2. Click your avatar in the upper right corner and selecting Settings
3. Navigateto SSH Keys in the left catalog
4. Go to your service, type ssh-keygen -t rsa -b 4096 -C "xujingyi@eduucas02" to set a rsa key, where the comment is to help you to know what is key belongs to
5. Then follow the screen, it will tell you what to do (I suggest to press enter to use the default path for keys and use no password for the keys)
6. If you have done all the steps above, you could find two files in ~/.ssh, one is id_rsa, the other is id_rsa.pub. The former is a private key for you service, just let it be there. The latter is a public key，you should paste its container to the site you just opened at step 3 via the command xclip -sel clip < ~/.ssh/id_rsa.pub.
7. After pasting, click add key. You could find your existing keys (for different ways) below.

# Create a project
1. Find a "plus" button in the top of the site, click new project.
2. Fill the form of blank project and create a project.
3. Open your project, find clone button in the top right, copy ssh link.

# share your files
1. Go back to your service and create a folder.
2. Type git clone ssh://git@gitlab.cern.ch:7999/jingyi/jingyi-project.git (with your project ssh link). You could find a project folder created.
3. If you want to submit something, copy your files to this project folder and  use these commands:
   git add . (will add all files within that folder; you can specify something like git add *.C, *.py, ...)
   git commit -a -m "SOME COMMENTS" (prepare files to be submitted to git, going to master branch)
   git push (files will be uploaded)

# FAQ
You could find some FAQ at the following link
https://gitlab.cern.ch/help/ssh/README#generating-a-new-ssh-key-pair
